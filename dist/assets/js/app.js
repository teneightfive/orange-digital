$(function() {
  'use strict';
  
  if ($('.js-carousel').length > 0) {
    $('.js-carousel').flexslider({
      slideshow: true,
      slideshowSpeed: 7000,
      animationSpeed: 700,
      directionNav: false
    });
  }

  $('.site-nav__btn').click(function (e) {
    e.preventDefault();
    $('.site-nav').toggleClass('is-open');
  });
});