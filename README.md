# Orange Digital FE Test

This repository contains the source files (`src/`) and distribution files (`dist/`) for a front end developer test for Orange Digital. 

## Dependencies

This app has certain dependencies in order to continue development or run distribution builds. Those are:

* [Node.js](http://nodejs.org/)
* [Grunt CLI](http://gruntjs.com/)
* [Bower](http://bower.io)
* [Ruby](https://www.ruby-lang.org/en/) + [Sass](http://sass-lang.com/)

Then, if planning to run any development or deployment scripts, run these commands first:

    npm install

    bower install

## Development env

Ensure all Dependencies listed above are install. Then to continue development of the source files run:

    grunt watch

This will compile the sass and js files used in `src/index.html`. 

## Deploy

Ensure all Dependencies listed above are install. Then compile the source files to create a deployable version run:

    grunt build

## Testing

Ensure all Dependencies listed above are install. Then run JSHint on the JS files run:

    grunt test

JSHint configuration can be found in `.jshintrc`.

## Staging env

The current version of the page can be seen on [heroku](http://www.heroku.com) at this address [warm-crag-8709.herokuapp.com/](http://warm-crag-8709.herokuapp.com/). This file is being served using node.js. The code for the server can be found in `app.js`.

