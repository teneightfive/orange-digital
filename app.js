var express = require('express');
var port = process.env.PORT || 3000;
var app = express();

// serve static files
app.use(express.static('./dist'));

// direct all requests to index.html
app.get('*', function(req, res) {
  res.sendfile('./dist/index.html');
});

// run server
app.listen(port);