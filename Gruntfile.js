module.exports = function(grunt) {
  'use strict';

  // Load all plugins
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		// Create watch task to watch Sass and JS directories during Dev
		watch: {
			css: {
				files: ['src/assets/css/**/*.scss'],
				tasks: ['sass:dev'],
			},
      js: {
        files: ['Gruntfile.js', 'src/js/mod/*', 'src/js/*.js'],
        tasks: ['jshint', 'concat:javascript'],
      }
		},
    // Empty out CSS and JS directories so they are clean for Grunt to compile correct files
    clean: {
      dist: ['dist/']
    },
    // build sass files
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: [{
          expand: true,
          cwd: 'src/assets/css/',
          src: ['**/*.scss'],
          dest: 'dist/assets/css',
          ext: '.min.css'
        }]
      },
      dev: {
        options: {
          style: 'expanded'
        },
        files: [{
          expand: true,
          cwd: 'src/assets/css/',
          src: ['*.scss'],
          dest: 'src/assets/css',
          ext: '.css'
        }]
      }
    },
    // Process the html
    processhtml: {
      release: {
        files: {
          'dist/index.html': ['src/index.html']
        }
      }
    },
    // copy images
    copy: {
      images: {
        files: [{
          expand: true, 
          cwd: 'src/assets/img', 
          src: ['**/*'], 
          dest: 'dist/assets/img'
        }]
      }
    },
		// Optimise images for production environment
		imagemin: {
			production: {
				options: {
					optimizationLevel: 5
				},
				files: [{
					expand: true,
					cwd: 'dist/assets/img',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'dist/assets/img'
				}]
			}
		},
    // JSHint JS files to flush out any errors
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        ignores: [
          // ignore plugins
          'src/assets/js/plugins/*'
        ]
      },
      files: [
        'Gruntfile.js',
        'src/assets/js/**/*.js'
      ]
    },
		// Concatinate all the JavaScript files to reduce amount of requests
		concat: {
			options: {
				// define a string to put between each file in the concatenated output
				separator: ';'
			},
			javascript: {
				// the files to concatenate
				src: [
          'src/assets/js/*.js'
        ],
				// the location of the resulting JS file
				dest: 'dist/assets/js/app.js'
			},

		},
		// Minify JavaScript files to reduce page weight
		// Only used for production environment
		uglify: {
			options: {

			},
			javascript: {
				files: {
          'dist/assets/js/app.min.js': ['<%= concat.javascript.dest %>'],
          // vendor files
          'dist/assets/vendor/flexslider/jquery.flexslider.js': ['src/assets/vendor/flexslider/jquery.flexslider.js'],
          'dist/assets/vendor/jquery/jquery.min.js': ['src/assets/vendor/jquery/jquery.min.js'],
          'dist/assets/vendor/modernizr/modernizr.js': ['src/assets/vendor/modernizr/modernizr.js'],
          'dist/assets/vendor/respond/dest/respond.min.js': ['src/assets/vendor/respond/dest/respond.min.js']
				}
			}
		}
	});

	// Tasks.
	grunt.registerTask('default', ['test', 'build']);
  grunt.registerTask('test', ['jshint']);
	grunt.registerTask('build', ['clean:dist', 'sass:dist', 'processhtml', 'copy:images', 'imagemin', 'concat', 'uglify']);
};